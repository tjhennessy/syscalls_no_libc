# Description
  
This program implements the cp utility using inline assembly and 64-bit syscenter  
calls.  These calls include open, read, write, lseek, and close.

NOTE: The fast system calls (syscenter) are significantly faster than the interrupt
method (0x80); however, they are not available for processors older than pentium II
and kernel versions less than 2.6.
  
# Usage
  
Run the following to compile 
```
./buildall
```
  
Run the command using
```
./syscp <src> <dst>
```
  
