#define SYSCALL_NUMBER_READ	    0
#define SYSCALL_NUMBER_WRITE	1
#define	SYSCALL_NUMBER_OPEN	    2
#define SYSCALL_NUMBER_CLOSE	3
#define SYSCALL_NUMBER_LSEEK    8
#define SYSCALL_NUMBER_EXIT	    60
#define SYSCALL_NUMBER_RENAME	82

/*
 * File status flags: these are used by open(2), fcntl(2).
 * They are also used (indirectly) in the kernel file structure f_flags,
 * which is a superset of the open/fcntl flags.  Open flags and f_flags
 * are inter-convertible using OFLAGS(fflags) and FFLAGS(oflags).
 * Open/fcntl flags begin with O_; kernel-internal flags begin with F.
 */
/* open-only flags */
#define	O_RDONLY	0x0000		/* open for reading only */
#define	O_WRONLY	0x0001		/* open for writing only */
#define	O_RDWR		0x0002		/* open for reading and writing */
#define	O_APPEND	00002000	/* set append mode */

#define O_CREAT		00000100	/* create if nonexistant */
#define	O_TRUNC		00001000	/* truncate to zero length */
#define	O_EXCL		00000200	/* error if already exists */

/* Values for the WHENCE argument to lseek.  */
#ifndef	_STDIO_H		/* <stdio.h> has the same definitions.  */
# define SEEK_SET	0	/* Seek from beginning of file.  */
# define SEEK_CUR	1	/* Seek from current position.  */
# define SEEK_END	2	/* Seek from end of file.  */
# ifdef __USE_GNU
#  define SEEK_DATA	3	/* Seek to next data.  */
#  define SEEK_HOLE	4	/* Seek to next hole.  */
# endif
#endif

/**
 * 64-bit syscall format
 * NUMBER	ABI	NAME	ENTRY POINT
 * ----------------------------------------
 * 0		common  read	sys_read
 * 1		common	write	sys_write
 * 2		common	open	sys_open
 * 3		common	close	sys_close
 * 60		common	exit	sys_exit
 * 82		common 	rename	sys_rename
 */

/* user defined strlen to avoid needing stdlib */
unsigned int slen(const char *str)
{
    const char *s;
    for (s = str; *s; ++s) {}
    return (s - str);
}

/* exit(int status) */
void my_exit(long status)
{
     asm("movq %0, %%rax\n"
        "movq %1, %%rdi\n"
	    "syscall"
	    : /* output params = none */
	    /* (none) */
	    : /* input params mapped to %0 and %1 */
	    "e" (SYSCALL_NUMBER_EXIT), "r" (status)
	    : /* clobbered registers */
	    "rax", "rdi"
	    );
}

/* write(int fd, const void *buf, size_t count) */
long int my_write(int fd, void *buf, unsigned int count)
{
    long int ret = 0;

    asm("movq %1, %%rax\n" /* syscall number */
    	"movq %2, %%rdi\n" /* fd */
    	"movq %3, %%rsi\n" /* buf */
    	"movl %4, %%edx\n" /* count */
    	"syscall"
    	: /* output params */
	    "=g" (ret)
    	: /* input params */
    	"e" (SYSCALL_NUMBER_WRITE), "g" (fd), "g" (buf), "g" (count)
       );

    return ret;
}

/* read(int fd, void *buf, size_t count) */
long int my_read(int fd, void *buf, unsigned int count)
{
    long int ret;

#ifdef DEBUG
    my_write(1, "before call to read: ", slen("before call to read: "));
    my_write(1, buf, count);
    my_write(1, "\n", 1);
#endif

    asm("movq %1, %%rax\n"
        "movq %2, %%rdi\n"
        "movq %3, %%rsi\n"
        "movq %4, %%rdx\n"
        "syscall"
        : "=g" (ret)
        : "e" (SYSCALL_NUMBER_READ), "g" (fd), "g" (buf), "g" (count)
        );

#ifdef DEBUG
    my_write(1, "my_read: ", slen("my_read: "));
    my_write(1, buf, count);
    my_write(1, "\n", 1);
#endif
    
    return ret;
}

/* lseek(int fd, long int offset, int whence) */
long int my_lseek(int fd, long int offset, int whence)
{
    long int offset_ret = 0;

    asm("movq %1, %%rax\n"
        "movq %2, %%rdi\n"
        "movq %3, %%rsi\n"
        "movq %4, %%rdx\n"
        "syscall"
        : "=g" (offset_ret)
        : "e" (SYSCALL_NUMBER_LSEEK), "g" (fd), "g" (offset), "g" (whence)
       );
    
    return offset_ret;
}

/* wrapper for lseek above which gets bytes and rewinds file offset */
long int get_file_size(int fd)
{
    long int count = 0;

    count = my_lseek(fd, SEEK_SET, SEEK_END);
    my_lseek(fd, 0L, SEEK_SET);

    return count;
}

/* open(const char *pathname, int flags) */
int my_open(const char *pathname, int flags)
{
    int fd = -1;

    asm("movq %1, %%rax\n"
        "movq %2, %%rdi\n"
        "movq %3, %%rsi\n"
        "syscall"
        : "=g" (fd)
        : "e" (SYSCALL_NUMBER_OPEN), "g" (pathname), "g" (flags)
    );

#ifdef DEBUG
    my_write(1, pathname, slen(pathname));
    my_write(1, "\n", 1);
#endif 

    return fd;
}

/* int open(const char *pathname, int flags, unsigned int mode)

 * Use this to create a file which does not exist.  The reason this
 * is necessary instead of using open is mode must be specified for
 * this use case.  The my_open function does not support it.
 */
int my_create(const char *pathname, int flags, unsigned int mode)
{
    int fd = -1;

    asm("movq %1, %%rax\n"
        "movq %2, %%rdi\n"
        "movq %3, %%rsi\n"
        "movq %4, %%rdx\n"
        "syscall"
        : "=g" (fd)
        : "e" (SYSCALL_NUMBER_OPEN), "g" (pathname), "g" (flags), "g" (mode)
    );

#ifdef DEBUG
    my_write(1, pathname, slen(pathname));
    my_write(1, "\n", 1);
#endif 

    return fd;
}

/* close(int fd) */
int my_close(int fd)
{
    int ret = 0;

    asm("movq %1, %%rax\n"
        "movq %2, %%rdi\n"
        "syscall"
        : "=g" (ret)
        : "e" (SYSCALL_NUMBER_CLOSE), "g" (fd)
        );
    
    return ret;
}

// TODO: implement these below
void my_rename(void);

int main(int argc, char *argv[])
{
    unsigned long syscall_num;
    int srcfd;
    int dstfd;
    long int num_bytes;
    char *err_msg;
    char buf[512];
    
    if (argc != 3)
    {
	    err_msg = "Must specify <src path> <dst path>\n";
	    my_write(1, err_msg, slen(err_msg));
        my_write(1, "\n", 1);
        my_exit(1);
    }

    srcfd = my_open(argv[1], O_RDONLY);  
    /* Create write only file only if it does not exist.
     * Mode required to avoid undefined behavior for files 
     * which do not yet exists
     */
    dstfd = my_create(argv[2], O_WRONLY | O_CREAT | O_APPEND, 0644); 

    num_bytes = get_file_size(srcfd);

    my_read(srcfd, buf, num_bytes);
    my_write(dstfd, buf, num_bytes);

    my_close(srcfd);
    my_close(dstfd);

    my_exit(0);
}

#ifdef __x86_64__
    asm(".global _start\n"
	"_start:\n"
	"   xorl %ebp,%ebp\n"
        "   movq 0(%rsp),%rdi\n"     
        "   lea 8(%rsp),%rsi\n"       
        "   call main\n"       
        "   movq %rax,%rdi\n"       
        "   movl $60,%eax\n"        
        "   syscall\n"
        "   int3\n"
	);       
#endif  

